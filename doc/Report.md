## (Toy) MIPS CPU

### Target
使用硬件语言来描述一个利用五级流水实现的CPU， 并要求实现[MIPS Instruction Set](https://en.wikipedia.org/wiki/MIPS_instruction_set#MIPS_assembly_language)里的所有指令。

### Environment
Sublime + iverilog + gtkwave

轻量级的iverilog配合gtkwave使用还是很舒适的。同时sublime上的 System Verilog插件支持的还是很nice，详情可以见了了的Report [ChenLequn's Report](https://github.com/abcdabcd987/toy-cpu/blob/master/doc/report.md)

### Usage
	ivertilog -o a_cute_name *.v
	vvp a_cute_name
想看波形可以
	
	gktwave test.vcd
或者直接打开gktwave操作GUI

注意vcd文件需要在test batch里面加入
	
	$dumpfile("test.vcd");
	
想要记录哪些变量可通过	

	$dumpvars(arg1, arg2);
	
###Design
架构设计大部分参考[自己动手写CPU](http://blog.csdn.net/leishangwen/article/category/5723475/2)。这是一套**Havard Structure**的CPU而不是Von Neumann，这样处理许多Hazards会容易许多。同时通过**Forwarding**解决数据hazard并且带有延迟槽。

###Feelings

通过写CPU的过程，我更好的了解了计算机电路的具体工作原理，感觉弥补了计算机体系结构中电路这一方面的知识，让我受益匪浅。再就是感觉到了硬件语言编写起来的困难。

###TIPS

一开始编写的时候我把存指令的数组开的非常大，然后编译时间超长，曾经最长6min。 后来发现开的比较小（够用）的情况下编译就非常快了。

### Experiment and Result
非常感谢宇榕对于测试数据作出的贡献，下面所有的测试数据获取自[宇榕的github](https://github.com/YurongYou/MIPS-CPU-Test-Cases)和[李亚民资料]()
#### Forwarding

![](https://bytebucket.org/bywbilly/mycpu/raw/f06c0ef1999df00b29e382a7ac9fc61f6c34f3a7/benchmark/Forward.png?token=466679b382640f69afa6d66a52a17fc65c76eef2)

#### Memory

![](https://bytebucket.org/bywbilly/mycpu/raw/f06c0ef1999df00b29e382a7ac9fc61f6c34f3a7/benchmark/Mem1.png?token=2457e2abfb8174313a0ce6cc398f394f52039836)

![](https://bytebucket.org/bywbilly/mycpu/raw/f06c0ef1999df00b29e382a7ac9fc61f6c34f3a7/benchmark/mem2.png?token=12cd037472d0ace5a9007285466b0e11b462b6ae)

![](https://bytebucket.org/bywbilly/mycpu/raw/f06c0ef1999df00b29e382a7ac9fc61f6c34f3a7/benchmark/mem3.png?token=8ee53f5454cd0f3a245604e384db33106990530c)

####arithmetic

![](https://bytebucket.org/bywbilly/mycpu/raw/f06c0ef1999df00b29e382a7ac9fc61f6c34f3a7/benchmark/arithmetic1.png?token=ee7788a45945a00532250011f03b5d797aa429a7)

![](https://bytebucket.org/bywbilly/mycpu/raw/f06c0ef1999df00b29e382a7ac9fc61f6c34f3a7/benchmark/arithmetic2.png?token=7aed1c04d07ea02f9459a8b6da9c6bcd818bd608)

![](https://bytebucket.org/bywbilly/mycpu/raw/f06c0ef1999df00b29e382a7ac9fc61f6c34f3a7/benchmark/arithmetic3.png?token=ccfa6985a5c684bc84338040b2569423034eb7af)

#### bitwise

![](https://bytebucket.org/bywbilly/mycpu/raw/f06c0ef1999df00b29e382a7ac9fc61f6c34f3a7/benchmark/bitwise1.png?token=121fddb6ec2e09048096697539ceb4ac96b9163b)

![](https://bytebucket.org/bywbilly/mycpu/raw/f06c0ef1999df00b29e382a7ac9fc61f6c34f3a7/benchmark/bitwise2.png?token=cc8bb8d883123ce99b8682b9a60db6494d6deab5)

#### branch

![](https://bytebucket.org/bywbilly/mycpu/raw/f06c0ef1999df00b29e382a7ac9fc61f6c34f3a7/benchmark/branch1.png?token=0ded50f3e85d5cdbc4133a84ca9ba659d6ea207e)

![](https://bytebucket.org/bywbilly/mycpu/raw/f06c0ef1999df00b29e382a7ac9fc61f6c34f3a7/benchmark/branch2.png?token=e79a46f27828c933b3ef0f4418829bfee1742a7b)

#### logic

![](https://bytebucket.org/bywbilly/mycpu/raw/f06c0ef1999df00b29e382a7ac9fc61f6c34f3a7/benchmark/logic1.png?token=c0de7884ccd9798bbcfe997851f7804f7c3fcc0e)

![](https://bytebucket.org/bywbilly/mycpu/raw/f06c0ef1999df00b29e382a7ac9fc61f6c34f3a7/benchmark/logic2.png?token=07a4d9be0b294ace2f5371c859cafd0f10ebc57d)

#### yamin

![](https://bytebucket.org/bywbilly/mycpu/raw/1fb4dea2d8707bc2b93bc114985a20db37da5821/benchmark/yamin1.png?token=e99a7f4775412fccb2d357225b08c5d4ea8cdf9a)

![](https://bytebucket.org/bywbilly/mycpu/raw/1fb4dea2d8707bc2b93bc114985a20db37da5821/benchmark/yamin2.png?token=c54b2cb339b2cbca5ac43205b181f4711a858eee)

![](https://bytebucket.org/bywbilly/mycpu/raw/1fb4dea2d8707bc2b93bc114985a20db37da5821/benchmark/yamin3.png?token=fef60699bad9920fd63d2c20a83ae4f3564faa5b)

![](https://bytebucket.org/bywbilly/mycpu/raw/1fb4dea2d8707bc2b93bc114985a20db37da5821/benchmark/yamin4.png?token=85a95eff1da426fe6ca7f124762d4e00d795f01f)

![](https://bytebucket.org/bywbilly/mycpu/raw/1fb4dea2d8707bc2b93bc114985a20db37da5821/benchmark/yamin5.png?token=98a4590dcb150c4a5b4b0f0b90509d3d7a53cc58)

###TODO
- 实现中断和异常
- 在板子上跑起来
- 尝试加入乱序执行
- 尝试加入**Branch Prediction**