`include "Define.v"
//`include "billymips_min_sopc.v"
`timescale 1ns/1ps
	
module billymips_min_sopc_tb();

reg CLOCK_50;
reg rst;

initial begin
	CLOCK_50 = 1'b0;
	forever #10 CLOCK_50 = ~CLOCK_50;
end

integer i;

initial begin
	$dumpfile("billytest.vcd");
	$dumpvars(0, billymips_min_sopc0.billymips0.regfile1.regs[1]);
	$dumpvars(0, billymips_min_sopc0.billymips0.regfile1.regs[2]);
	$dumpvars(0, billymips_min_sopc0.billymips0.regfile1.regs[3]);
	$dumpvars(0, billymips_min_sopc0.billymips0.regfile1.regs[4]);
	$dumpvars(0, billymips_min_sopc0.billymips0.regfile1.regs[5]);
	rst = `RstEnable;
	#195 rst = `RstDisable;
	#5000 $stop;	
end


billymips_min_sopc billymips_min_sopc0(
	.clk(CLOCK_50),
	.rst(rst)
);
endmodule
